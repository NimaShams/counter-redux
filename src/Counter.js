import React, {Component} from "react";
import {connect} from 'react-redux'

class Counter extends Component{
    Increment = () => {
        this.props.dispatch({
            type : 'INCREMENT'
        })
    }

    Decrement = () =>{
        this.props.dispatch({
            type : 'Decrement'
        })
    }
    render() {
        return (
            <div>
                <h2>Counter</h2>
                <div>
                    <button onClick={this.Increment}>+</button>
                    <span>{this.props.count}</span>
                    <button onClick={this.Decrement}>-</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        count : state.count
    }
}
export default connect(mapStateToProps)(Counter);