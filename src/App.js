import React from 'react';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import './App.css';
import Counter from "./Counter";
import Reducer from "./reducer";

class App extends React.Component{

    Store=createStore(Reducer);
    render() {
        return (
            <Provider className="App" store={this.Store}>
                <Counter />
            </Provider>
        );
    }


}

export default App;
